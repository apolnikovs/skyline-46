<?php

//IMPORTANT: this controller is to be accessed by server only for the purporse of running cron jobs.
//Anything related to login/session functionality should never be used here.

require_once('CustomController.class.php');


class CronController extends CustomController {
    
    
    
    public $config;  
    public $messages;
    public $user; //addet user variable Andris Polnikovs 24/02/2013
    
    
    
    public function __construct() { 
        
        parent::__construct(); 
        
        $this->config = $this->readConfig('application.ini');
        
        $this->messages = $this->loadModel('Messages'); 
        $this->user=new stdClass();
        $this->user->ServiceProviderID=0;
        
        $GLOBALS['IS_CRON_JOB'] = true;
        
    }
       
    
    
    //Sends open jobs report to Service Providers (intended for a cron job)
    //2013-01-02 © Vic <v.rutkunas@pccsuk.com>
    
    public function sendASCServiceProviderReportAction() {
	
	set_time_limit(300);
	$reportModel = $this->loadModel("Reports");
	$reportModel->sendASCServiceProviderReport();
	
    }

    
    
    //Sends open jobs report to Networks (intended for a cron job)
    //2013-01-02 © Vic <v.rutkunas@pccsuk.com>
    
    public function sendASCNetworkReportAction() {
	
	set_time_limit(300);
	$reportModel = $this->loadModel("Reports");
	$reportModel->sendASCNetworkReport();
	
    }
    
    
    
    //Replicate enginners week data to next week
    //2013-02-03 © Nag <nag.phpdeveloper@gmail.com>
    
    public function engineerReplicateSetupAction() {
	
        
       // $Today = "2013-02-10";//For testing.
        $Today = date("Y-m-d");
        
        if(date("N", strtotime($Today))==7)
        {
        
            $EngineersModel = $this->loadModel("Engineers");
            $EngineersResult = $EngineersModel->fetchReplicateEngineers();

             
             $CurrentWeek = $TargetWeek = array();
             for($i=6;$i>=0;$i--)
             {
                 $CurrentWeek[] = date("Y-m-d", strtotime('-'.$i.' day', strtotime($Today)));
             }
             
             //Target Week
             for($i=22;$i<=28;$i++)
             {
                 $TargetWeek[] = date("Y-m-d", strtotime('+'.$i.' day', strtotime($Today)));
             }
             
            // $this->log($CurrentWeek);
            // $this->log($TargetWeek);

             foreach ($EngineersResult AS $Eng)
             {
                 for($i=0;$i<=6;$i++)
                 {
                    
                    $dataArgs   = array('ServiceProviderEngineerID'=>$Eng['ServiceProviderEngineerID'], 'WorkDate'=>$CurrentWeek[$i]);
                    
                    
                    $EngDetails = $EngineersModel->fetchEngineerDetails($dataArgs);
                    
                    //$this->log($EngDetails);
                    
                    $TargetWeekEngDetails = $EngineersModel->fetchEngineerDetails(array('ServiceProviderEngineerID'=>$Eng['ServiceProviderEngineerID'], 'WorkDate'=>$TargetWeek[$i]));
                    
                    //$this->log('--->dd');
                    //$this->log($TargetWeekEngDetails);
                    
                    
                    if(is_array($EngDetails) && count($EngDetails) && !count($TargetWeekEngDetails))
                    {    
                        
                        
                        if($Eng['ReplicateStatusFlag']!='Yes')
                        {
                            $EngDetails['Status'] = 'In-active';
                        }
                        
                        
                        $EngSkillset = $EngineersModel->fetchEngineerSkillSet($EngDetails['ServiceProviderEngineerDetailsID']);

                        $DataArray  = array('ServiceProviderEngineerID'=>$Eng['ServiceProviderEngineerID'], 'WorkDate'=>$TargetWeek[$i], 'StartShift'=>$EngDetails['StartShift'], 'EndShift'=>$EngDetails['EndShift'], 'StartPostcode'=>$EngDetails['StartPostcode'], 'EndPostcode'=>$EngDetails['EndPostcode'], 'Status'=>$EngDetails['Status']);

                        $ServiceProviderEngineerDetailsID = $EngineersModel->processEngineerDetails($DataArray);

                        if($ServiceProviderEngineerDetailsID)
                        {
                            $EngineersModel->insertSkillSet($EngSkillset, $ServiceProviderEngineerDetailsID);
                        }    
                    }    
                       
                    
                 }
                 
                 
                 
                 //Replicate Postcode Allocation
                 if($Eng['ReplicatePostcodeAllocation']=='Yes')   
                 {  
                     
                    $EngineersModel->replicatePostcodeAllocation($Eng['ServiceProviderEngineerID'], $Eng['ServiceProviderID'], true);
                 }
                 
             }    
        }
        else
        {
            echo "This method runs on Sundays only.";
        }
    }
    
    
    
    //Process Engineers Daily 
    // © Andris Polnikovs <a.polnikovs@gmail.com>
    //version->1=>2013-02-24
    
    public function processDailyEngineersAction() {
        set_time_limit(999);
         $diary=$this->loadModel('Diary');
         $diary->processDailyEngineers();
    
    }
    
    
    
    //Process Viamente every X minutes for all next 6 days
    // © Andris Polnikovs <a.polnikovs@gmail.com>
    //version->1=>2013-03-12
    
    public function processViamenteAction($args) {
        set_time_limit(0);
         $diary=$this->loadModel('Diary');
         $diary->processViamente($args);
    
    }
    
    
    
    //send emails every X minutes for allocation only
    // © Andris Polnikovs <a.polnikovs@gmail.com>
    //version->1=>2013-03-26
    
    public function sendAllocationOnlyEmailAction($args) {
        set_time_limit(0);
         $diary=$this->loadModel('Diary');
         $diary->allocationOnlyEmails();
    
    }
    
    
    
    //Sending appointment reminder emails..
    //2013-04-02 © Nag <nag.phpdeveloper@gmail.com>
    public function sendAppointmentReminderEmailAction($args) {
         
        // $appointment_model = $this->loadModel('Appointment');
    
    }
    
    
    
    /**
     * pollSwapitJobResponse
     * 
     * poll for FTP response files from all DPD couriers.
     * 
     * @return 
     * 
     * @author brian Etherington <b.etherington@pccsuk.com>
     **************************************************************************/
    public function pollSwapitJobResponsesAction() {
        
        // REMOTE_ADDR no longer works behind the new load balancing proxy
        // so remove this security check. 
        //if( $_SERVER["REMOTE_ADDR"] != '127.0.0.1' )
       //     throw new Exception('Execution not from localhost');
        
        set_time_limit(0);
        $courierDPD_model = $this->LoadModel('CourierDPD');
        $courierDPD_model->pollSwapitJobResponses();
   
    }
    
    /**
     * satisfactionary Questionnaires Status update
     * 
     * Implementation Date is equal to Current Date Then change the status to      * 
     * Inactive of privious questionnairs which is less than implementation date   * 
     * update Termination Date as yesterday                                        * 
     * Tracker log 357 
     * 
     * @author v srinivasa rao <s.vempati@pccsuk.com>
     **************************************************************************/
    public function statusChangeQuestionnaireBrandAction() {
        
        set_time_limit(0);
        $Questionnaire         = $this->loadModel('Questionnaire');
        $Questionnaire->cronStatussChangeQuestionnaireBrand();
   
    }
    
    
}


?>