<?php

require_once('CustomSmartyController.class.php');

class PopupController extends CustomSmartyController {
    public $debug = false;
    public $config;  
    public $session;
    public $skyline;
    public $messages;
    public $user;
    private $lang = 'en';
    private $statuses  = array(
                     
                     0=>array('Name'=>'Active', 'Code'=>'Active'),
                     1=>array('Name'=>'In-Active', 'Code'=>'In-active')
                    
                     );    
    
    public function __construct() { 
        
        parent::__construct(); 
        
        /* ==========================================
         * Read Application Config file.
         * ==========================================
        */        
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
        *  Initialise Session Model
        * ==========================================
        */
               
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
        $this->messages = $this->loadModel('Messages'); 
        
        /* ==========================================
         *  Initialise Skyline Model
         * ==========================================
         */
        $this->skyline = $this->loadModel('Skyline');         
        
        //$this->smarty->assign('_theme', 'skyline');
        
        /* ==========================================
         *  Initialise User Class
         * ==========================================
         */
        if (isset($this->session->UserID)) {
            
            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
            
            $this->smarty->assign('name',$this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;
            
        } else {
           $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('name','');
            $this->smarty->assign('last_logged_in','');
            $this->smarty->assign('logged_in_branch_name', '');
            $this->smarty->assign('_theme', 'skyline');
            
        }  
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        
        if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
        
        $this->smarty->assign('showTopLogoBlankBox', false);
        
//        if ($_SERVER['REQUEST_METHOD'] != 'POST') { 
//            throw new Exception('Data request is not POST');
//        }
                       
    }
       
    public function updateAction(){
        return array('status' => 'OK',
                     'message' => '');        
    }
    
    public function viewBookedByAction( $args ) {       
        
        if (!isset($this->session->UserID)) throw new Exception('User Not Logged In');
        
        $JobID = isset($args[0]) ? $args[0] : '';
        
        $page = $this->messages->getPage('bookedBy', $this->lang);
        $this->smarty->assign('page', $page);
        
        $skyline_business_model = $this->loadModel('SkylineBusinessModel');
        $data = $skyline_business_model->getJobDetails( $JobID );
        
        $this->smarty->assign('network', $data['network'] );
        $this->smarty->assign('client', $data['client'] );
        $this->smarty->assign('user', $data['user'] );
        
        $html_code = $this->smarty->fetch('popup/ViewBookedBy.tpl');
        
        echo $html_code;
        
    }
    
    public function viewServiceCentreAction( $args ){
        $servericeCentreID = isset($args[0]) ? $args[0] : '';
        
        $skyline_model = $this->loadModel('Skyline');
        //$service_providers_model = $this->loadModel('ServiceProviders');
        
        //$this->log(var_export($skyline_model->getServiceCenterDetails( $servericeCentreID), true));
        $this->smarty->assign($skyline_model->getServiceCenterDetails( $servericeCentreID) );
        
        $html_code = $this->smarty->fetch('popup/ViewServiceCentre.tpl');
        
        echo $html_code;
    }
    
    public function editServiceCentreAction( $args ){
        
        $networkID = isset($args[0]) ? $args[0] : '';
        $serviceCentreID = isset($args[1]) ? $args[1] : '';
        
        $page = $this->messages->getPage('serviceProviders', $this->lang);
        $this->smarty->assign('page', $page);
        
        $Skyline    = $this->loadModel('Skyline');
        $vat_rates_model = $this->loadModel('VATRates');
            
        $countries  = $Skyline->getCountriesCounties();
        $vatRates = $vat_rates_model->getCountryVatRates();
        $networks   = $Skyline->getNetworks();
        $platforms   = $Skyline->getPlotforms();

        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('countries', $countries);
        $this->smarty->assign('vatRates', $vatRates);
        $this->smarty->assign('networks', $networks);
        $this->smarty->assign('platforms', $platforms);

        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
        $this->smarty->assign('userPermissions', $this->user->Permissions);//this value should be from user calss
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;

        $model = $this->loadModel('ServiceProviders');

        $datarow  = $model->fetchRow(array( 'NetworkID' => $networkID, 'ServiceProviderID' => $serviceCentreID));

        if($serviceCentreID && !$datarow['NetworkID']) {
            $datarow['NetworkID']   = $networkID;
            $datarow['NetworkName'] = $Skyline->getNetworkName($networkID);
            $datarow['InvoiceToCompanyName']   = '';
            $datarow['SageSalesAccountNo']   = '';
            $datarow['SagePurchaseAccountNo']   = '';
            $datarow['TrackingUsername']   = '';
            $datarow['TrackingPassword']   = '';
            $datarow['WarrantyUsername']   = '';
            $datarow['WarrantyPassword']   = '';
            $datarow['WarrantyAccountNumber']   = '';
            $datarow['Area']   = '';
            $datarow['WalkinJobsUpload']   = '';
            $datarow['CompanyCode']   = '';
            $datarow['AccountNo']   = '';
        }                           
                          
        $this->smarty->assign('form_legend', $page['Text']['update_page_legend']);

        $this->smarty->assign('datarow', $datarow);

        //Checking user permissons to display the page.
        if($this->user->SuperAdmin) {
            $accessErrorFlag = false;
        } else {
            $accessErrorFlag = true;
        }    

        $nClients        = $model->getServiceProviderClients($serviceCentreID);
        $clientContacts  = $model->getServiceProviderClientContacts($serviceCentreID);

        $this->smarty->assign('accessErrorFlag', $accessErrorFlag);
        $this->smarty->assign('nClients', $nClients);
        $this->smarty->assign('clientContacts', $clientContacts);
               
        //$html_code = $this->smarty->fetch('popup/editServiceCentre.tpl');
        $html_code = $this->smarty->fetch('serviceProvidersForm.tpl');
        
        echo $html_code;
    }

    /**
     * 
     * 
     * @param array $args
     * @return array
     *
     * @author Simon Tsang s.tsang@pccsuk.com	 
     ********************************************/
    public function addAppointmentAction( $args ){
      $jobID = isset($args[0]) ? $args[0] : '';
        
      $appointment_model = $this->loadModel('Appointment');
      $this->smarty->assign( 'Appointment', $appointment_model );
//        $appointment_model->getFields();
        $page = $this->messages->getPage('Appointment', $this->lang);
        $this->smarty->assign('page', $page);
        
        $default = array(

            'AppointmentDate'=>'0000-00-00',
            'AppointmentTime'=>'00:00'
        );
        
        $this->smarty->assign('AppointmentTypeList', $appointment_model->getAppointmentType());
        $this->smarty->assign('form_legend', $page['Text']['form_legend']);

        $this->smarty->assign('Default', $default);        

        $html_code = $this->smarty->fetch('popup/AppointmentsAdd.tpl');
        
        echo $html_code;
    }
    
    /**
     * 
     * 
     * @param array $args
     * @return array
     *
     * @author Simon Tsang s.tsang@pccsuk.com	
     * 
     * @todo require email field 
     ********************************************/
    public function editCustomerDetailsAction( $args ){
        $customerID = isset($args[0]) ? $args[0] : '';
       
        $this->smarty->assign( 'Skyline', $this->skyline );

        #$job_model = $this->loadModel('Customer');
        $this->loadModel('Customer');
        $customer_model = new Customer($this);
        $customer_model->CustomerID = $customerID;
        $customer_model->setSkyline( $this->skyline );
        
        $this->smarty->assign( $customer_model->details() );        
        
        
        # get labels from message model
        $job_model = $this->loadModel('Job'); 
        $page = $this->messages->getPage('Job', $this->lang);
        $this->smarty->assign('page', $page);

        $html_code = $this->smarty->fetch('popup/EditCustomerDetails.tpl');
        
        echo $html_code;        
    }

    
    
    public function editDeliveryDetailsAction($args) {
	
	$jobID = isset($args[0]) ? $args[0] : '';

	$this->smarty->assign( 'Skyline', $this->skyline );

	$job_model = $this->loadModel('Job');
	
	$details = $job_model->getDeliveryDetails($jobID);
	
	//$this->loadModel('Customer');
	//$customer_model = new Customer($this);
	//$customer_model->CustomerID = $customerID;
	//$customer_model->setSkyline( $this->skyline );

	//$this->smarty->assign( $customer_model->details() );        

	$this->smarty->assign($details);        
	
	$page = $this->messages->getPage('Job', $this->lang);
	$this->smarty->assign('page', $page);

	$html_code = $this->smarty->fetch('popup/EditDeliveryDetails.tpl');

	echo $html_code;        
	
    }
    

    
    // left from dataController
    public function productAction( $args ) {
                    
        $model = $this->loadModel('Product');
  
        $function = isset($args[0]) ? $args[0] : '';
        
        if( method_exists($model, $function) ){
            $result = $model->$function($_POST);
        }else{
            $result = array('status' => 'ERROR', 'message' => 'Unrecognised argument');
        }
        
        echo json_encode($result);
  
    }
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to manuplate data of Help Text.
     * 
     * @param array $args
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
       
      public function helpTextAction(  $args  ) {
          
          
              
            
            if ($_SERVER['REQUEST_METHOD'] == 'POST' && $this->user->SuperAdmin)
            {
                $HelpTextModel   = $this->loadModel('HelpText');
                $result          = $HelpTextModel->processData($_POST);
                echo json_encode($result);
            }
            else
            {
                $selectedRowId        = isset($args[0])?$args[0]:false;
                if(!$selectedRowId)
                {
                   $this->redirect('index',null, null); 
                }
            
                $this->page      = $this->messages->getPage('helpText', $this->lang);
                $this->smarty->assign('page', $this->page);



                $args['HelpTextCode'] = $selectedRowId;
                $model     = $this->loadModel('HelpText');

                $datarow   = $model->fetchRow($args);

                if(is_array($datarow) && isset($datarow['HelpTextID']))
                {
                    $this->smarty->assign('HelpTextTitle', $datarow['HelpTextTitle']);
                    $this->smarty->assign('HelpText', $datarow['HelpText']);
                }
                else
                {
                     $datarow = array('HelpTextID'=>'', 'HelpTextCode'=>$selectedRowId, 'HelpTextTitle'=>'', 'HelpText'=>'', 'Status'=>'Active');  
                
                     
                     $this->smarty->assign('HelpTextTitle', 'Help Title');
                     $this->smarty->assign('HelpText', 'Help Text');
                }


                $this->smarty->assign('form_legend', $this->page['Text']['legend']);


                $this->smarty->assign('SuperAdmin', ($this->user->SuperAdmin)?true:false);
                $this->smarty->assign('datarow', $datarow);
                
               
                $mode="";
                if(isset($args['inpopup'])){$mode="inpopup";}
                if(isset($args['Qtip'])){$mode="Qtip";}
              
                switch($mode){
                    case "inpopup": $htmlCode = $this->smarty->fetch('./popup/HelpTextPopUp.tpl');break;
                    case "Qtip": $htmlCode = $this->smarty->fetch('./popup/HelpTextQtip.tpl');break;
                    default : $htmlCode = $this->smarty->fetch('./popup/HelpText.tpl');break;
                }
                


                echo $htmlCode;    
          }
                  
      }
      
      
      
       /**
     * Description
     * 
     * This method is used for to display Unit Type Entry.
     * 
     * @param array $args
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
       
      public function unitTypeEntryAction(  $args  ) {
          
          
                
            
                $this->page      = $this->messages->getPage('newUnitType', $this->lang);
                $this->smarty->assign('page', $this->page);



                $this->smarty->assign('form_legend', $this->page['Text']['legend']);

                

                $htmlCode = $this->smarty->fetch('./popup/NewUnitType.tpl');


                echo $htmlCode;    
          
                  
      }
      
      
      
      /**
     * Description
     * 
     * This method is used for to display New mobile number entry.
     * 
     * @param array $args
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
       
      public function newMobileNumberAction(  $args  ) {
          
                
                $mobileNumber = (isset($args['mNo']))?$args['mNo']:false;
                $ssct = (isset($args['ssct']))?$args['ssct']:false;
                $ContactAltMobile1 = (isset($args['aMNo']))?$args['aMNo']:false;
                
            
                $this->page      = $this->messages->getPage('NewMobileNumber', $this->lang);
                $this->smarty->assign('page', $this->page);


                $this->smarty->assign('mobileNumber', $mobileNumber);
                $this->smarty->assign('ssct', $ssct);
                $this->smarty->assign('ContactAltMobile1', $ContactAltMobile1);
                

                $this->smarty->assign('form_legend', $this->page['Text']['legend']);

                

                $htmlCode = $this->smarty->fetch('./popup/NewMobileNumber.tpl');


                echo $htmlCode;    
          
                  
      }
      
      
       /**
     * Description
     * 
     * This method is used for to display Warranty Warning entry.
     * 
     * @param array $args
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
       
      public function warrantyWarningAction(  $args  ) {
          
                
                
                $this->page      = $this->messages->getPage('Job', $this->lang);
                $this->smarty->assign('page', $this->page);


              

                $this->smarty->assign('form_legend', $this->page['Text']['manufacturers_warranty_period']);
                

                $htmlCode = $this->smarty->fetch('./popup/WarrantyWarning.tpl');


                echo $htmlCode;    
          
                  
      }
    
       /**
    * RemoteEngineerImage
    * 
    * Display ty
    * 
    * @param array $args - 0    The Contact History ID
    * @return void
    * 
    * @author Andrew J. Williams <a.williams@pccsuk.com>
    ***************************************************************************/
    
    public function RemoteEngineerImageAction($args) {
        $contact_history_model = $this->loadModel('ContactHistory');
        
        if ( !isset($args[0]) ) {
            return;                                                             /* No contact history id passed so nothing to do */
        }

        $rec = $contact_history_model->GetImage($args[0]);
        if ($this->debug) $this->log($rec);
        
        $this->smarty->assign($rec);
        $html_code = $this->smarty->fetch('popup/RemoteEngineerImage.tpl');
        
        echo $html_code;
    }
    
    //this function is used to display standard skyline promt messages with 2 options yes or no
    //all params needs to be set as arguments none of params are compulsory and only needet args can be 
    //used
    //Andris 
    public function showGenericCBPromtAction($args){
     
        $popupData=[
            'legend'=>isset($args['legend'])?$args['legend']:'',
            'text'=>isset($args['text'])?$args['text']:'',
            'confButAction'=>isset($args['confButAction'])?$args['confButAction']:'',
            'confButText'=>isset($args['confButText'])?$args['confButText']:'',
            'cancelButAction'=>isset($args['cancelButAction'])?$args['cancelButAction']:'',
            'cancelButText'=>isset($args['cancelButText'])?$args['cancelButText']:'',
            'inputID'=>isset($args['inputID'])?$args['inputID']:'',
            'inputLabel'=>isset($args['inputLabel'])?$args['inputLabel']:'',
            'oneBut'=>isset($args['oneBut'])?$args['oneBut']:'',
        ];
        $this->smarty->assign('popupData',$popupData);
        $this->smarty->display('popup/GenericCBPromt.tpl');
    }
    

}

?>
