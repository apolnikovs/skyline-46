<?php

/**
 * APIJobs.class.php
 * 
 * Database access routines for the Users API
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.01
 * 
 * Changes
 * Date        Version Author                Reason
 * 15/06/2012  1.00    Andrew J. Williams    Initial Version
 * 13/03/2013  1.01    Andrew J. Williams    Issue 259 - MySQL Error in PutJobDetails for Client Account Number with String 
 ******************************************************************************/

require_once('CustomModel.class.php');

class APIUsers extends CustomModel {
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
    /**
     * Description
     * 
     * Check Validity of ClientAccountNumber againts user
     * 
     * @param string $cAcNo                     Client Account Number
     *        string $uId                       User ID
     *        string $uType                     Type of user {"Network", "ServiceProvider", "Client", "Branch"}
     * 
     * @return checkUserValidClientAccountNo    Boolean result based on validity
     */
    
    public function checkUserValidClientAccountNo($cAcNo, $uId, $uType) {
        switch ($uType) {
            case "Network":
                $sql = "
                        SELECT
                            clt.`AccountNumber`
                        FROM
                            `client` clt,
                            `network_client` nc,
                            `user` u
                        WHERE
                            u.`NetworkID` = nc.`NetworkID`
                            AND nc.`ClientID` = clt.`ClientID`
                            AND u.`UserID` = $uId
                            AND clt.`AccountNumber` = '$cAcNo'
                        ";
                break;
            
            case "ServiceProvider":
                $sql = "
                        SELECT
                            clt.`AccountNumber`
                        FROM
                            `client` clt,
                            `network_client` nc,
                            `network_service_provider` nsp,
                            `user` u
                        WHERE
                            u.`ServiceProviderID` = nsp.`ServiceProviderID`
                            AND nsp.`NetworkID` = nc.`NetworkID`
                            AND nc.`ClientID` = clt.`ClientID`
                            AND u.`UserID` = $uId
                            AND clt.`AccountNumber` = '$cAcNo'
                        ";
                break; 
            
            case "Client": 
                $sql = "
                        SELECT
                            clt.`AccountNumber`
                        FROM
                            `client` clt,
                            `user` u
                        WHERE
                            u.`ClientID` = clt.`ClientID`
                            AND u.`UserID` = $uId
                            AND clt.`AccountNumber` = '$cAcNo'
                        ";
                break;
            
            case "Branch":
                $sql = "
                        SELECT
                            clt.`AccountNumber`
                        FROM
                            `client` clt,
                            `client_branch` cltbrch,
                            `user` u
                        WHERE
                            u.`BranchID` = cltbrch.`BranchID`
                            AND cltbrch.`ClientID` = clt.`ClientID`
                            AND u.`UserID` = $uId
                            AND clt.`AccountNumber` = '$cAcNo'
                        ";
                break;
                
            default:
                /* Unknown return false */
                return(false);
        }
        
        $result = $this->Query($this->conn, $sql);
        
        return ( count($result) > 0 );
    }
        
    
}