<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of PartFaultCodes Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0 
 * @created     11/06/2013
 */
class PartFaultCodesLookups extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
       
        $this->fields = [
            "PartFaultCodeLookupID",
            "LookupName",
            "FaultCodeDescription",
            "Status",
            "ManufacturerID",
            "FieldNumber",
            "ForceJobFaultCode",
            "JobFaultCodeNo",
            "RestrictLookup",
            "RestrictLookupTo",
            "JobTypeAvailability",
        ];
            $this->relFields =[
             "PartFaultCodeLookupID",   
             "ModelID",            
        ];   
    }

    public function insertPartFaultCodesLookups($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
        //$P["Models"] = $Models;
       
        $id = $this->SQLGen->dbInsert('part_fault_code_lookup', $this->fields, $P, true, true);
       
   
        if($id !="")
        {
            
         $pID =array($this->conn->lastInsertId()); // Fetching the last insetedID from part_fault_lookup table
         
        //$P["PartFaultCodeLookupID"]=$pID;       
        
          foreach ($P["Models"] as $value) {
          
            $ModelsId = explode("," , $value); // converting the models values into array
           $params = array_merge($pID,$ModelsId); // merging two arrays 
           $id = $this->SQLGen->dbInsert('part_fault_code_lookup_model', $this->relFields, $params, false, false);  
           
        
            }

        }
        
        //$this->log($id);
        return $id;
    }

    public function updatePartFaultCodesLookups($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
        $id = $this->SQLGen->dbUpdate('part_fault_code_lookup', $this->fields, $P, "PartFaultCodeLookupID=" . $P['PartFaultCodeLookupID'], true);
        //$this->log($id);

        $pID =array($P['PartFaultCodeLookupID']); //Taking PartFaultCodeLookupID into array
            $pMID = $P['PartFaultCodeLookupID'];
            
        //delete the selected model numbers 
            $sql = "delete from part_fault_code_lookup_model where PartFaultCodeLookupID = $pMID "  ;
            $this->execute($this->conn, $sql);
            
        // inserted new model numbser based on manufacturer ID    
            foreach ($P["Models"] as $value) {
                
            $ModelsId = explode("," , $value);// converting the models values into array            
            $params = array_merge($pID,$ModelsId);// merging two arrays 
            
            $id = $this->SQLGen->dbInsert('part_fault_code_lookup_model', $this->relFields, $params, "PartFaultCodeLookupID=" . $P['PartFaultCodeLookupID'], false);  
         
        }
        
        
    }

    public function getPartFaultCodesLookupsData($id) {
        $sql = "select * from part_fault_code_lookup where PartFaultCodeLookupID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0];
    }
    
    // fetching model numbers based on partFaultCOdeLookupID using join.joined two table Model and part_fault_code_lookup_model
        public function getPartFaultCodesLookupsModelData($id) {
            
        $table="part_fault_code_lookup_model";
        $sql = "select * from part_fault_code_lookup_model join  model m on m.ModelID=$table.ModelID and $table.PartFaultCodeLookupID=$id ";
        $res = $this->query($this->conn, $sql);
        return $res;
    }
   
    public function deletePartFaultCodesLookups($id) {
        $sql = "update part_fault_code_lookup set Status='In-Active' where PartFaultCodeLookupID=$id";
        $this->execute($this->conn, $sql);
    }

    ////part_fault_code functions 

    public function checkIfOrderSection($id) {
        $sql = "select DisplayOrderSection from part_fault_code where PartStatusID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0]['DisplayOrderSection'];
    }

    public function addLinkedItem($p, $c) {
        $this->log($p);
        $u = $this->controller->user->UserID;
        $sql = "insert into part_fault_code_lookup_model (PartFaultCodeLookupModelID,PartFaultCodeLookupID,ModelID) values ($p,$c,$u)";
       
        //        $sql = "insert into part_fault_code_lookup_to_part_fault_code  (PartFaultCodesLookupID,PartFaultCodesLookupLookupID,ModifiedUserID) values ($p,$c,$u)";

        $this->execute($this->conn, $sql);
    }

    public function delLinkedItem($p, $c) {
        $sql = "delete from part_fault_code_lookup_to_part_fault_code where PartFaultCodesLookupID=$p and PartFaultCodesLookupLookupID =$c";
        $this->execute($this->conn, $sql);
    }

    public function loadLinkedItem($p) {
        $u = $this->controller->user->UserID;
        if ($this->controller->user->SuperAdmin == 1) {
            $t = "part_fault_code_lookup";
            $m = "PartFaultCodesLookupID";
        } else {
            $t = "part_fault_code_lookup_model";
            $m = "PartFaultCodesLookupModelID";
        }
        $sql = "select am.PartFaultCodesLookupModelID, 
            LookupName as `ItemName`  from part_fault_code_lookup am
                join $t m on m.$m=am.PartFaultCodesLookupID

            where PartFaultCodesLookupID=$p";
        return $this->Query($this->conn, $sql);
    }

}

?>