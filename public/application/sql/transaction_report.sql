SELECT	    job.JobID,
			branch.BranchName,
		    customer.ContactLastName,
		    CONCAT(customer_title.Title, ' ', customer.ContactFirstName) AS CustomerName,
		    manufacturer.ManufacturerName,
		    CASE
				WHEN unit_type.UnitTypeName IS NULL
				THEN job.ServiceBaseUnitType
				ELSE unit_type.UnitTypeName
		    END AS UnitTypeName,
		    CASE
				WHEN model.ModelNumber IS NULL
				THEN job.ServiceBaseModel
				ELSE model.ModelNumber
		    END AS ModelNumber,
		    job.SerialNo,
		    job.ImeiNo,
		    job.AgentRefNo,
		    service_type.ServiceTypeName,
		    job.DateBooked,
		    job.TimeBooked,
		    job.ClosedDate,
		    
		    @turnaround := turnaround(job.JobID) AS TAT,
		    
		    CASE 
				WHEN job.ClosedDate IS NULL
				THEN DATEDIFF(CURRENT_DATE, job.DateBooked)
				ELSE DATEDIFF(job.ClosedDate, job.DateBooked)
		    END AS DaysTAT,

		    status.StatusName,
		    job.ReportedFault,
		    job.RepairDescription,
		    1 AS Count,
		    
		    CASE
				WHEN job.ServiceAction = 'software_update'
				THEN 1
				ELSE NULL
		    END AS SoftwareUpdate,
		    CASE
				WHEN job.ServiceAction = 'education'
				THEN 1
				ELSE NULL
		    END AS CustomerEducation,
		    CASE
				WHEN job.ServiceAction = 'setup'
				THEN 1
				ELSE NULL
		    END AS Setup,
		    CASE
				WHEN job.ServiceAction = 'damaged'
				THEN 1
				ELSE NULL
		    END AS DamagedBeyondRepair,
		    CASE
				WHEN job.ServiceAction = 'faulty_acc'
				THEN 1
				ELSE NULL
		    END AS FaultyAccessory,
		    CASE
				WHEN job.ServiceAction = 'non_uk_mod'
				THEN 1
				ELSE NULL
		    END AS NonUKModel,
		    CASE
				WHEN job.ServiceAction = 'unresolvable'
				THEN 1
				ELSE NULL
		    END AS Unresolvable,
		    CASE
				WHEN job.ServiceAction = 'branch_repair'
				THEN 1
				ELSE NULL
		    END AS BranchRepair,
		    CASE
				WHEN job.ServiceAction = 'repair_required'
				THEN 1
				ELSE NULL
		    END AS RepairRequired,
		    
		    service_provider.CompanyName,
		    courier.CourierName,
		    shipping.ConsignmentNo,
		    job.ChargeableLabourCost,
		    job.ChargeablePartsCost,
		    job.ChargeableDeliveryCost,
		    job.ChargeableVATCost,
		    job.ChargeableTotalCost
	
FROM	    job

LEFT JOIN   customer ON job.CustomerID = customer.CustomerID
LEFT JOIN   customer_title ON customer.CustomerTitleID = customer_title.CustomerTitleID
LEFT JOIN   manufacturer ON job.ManufacturerID = manufacturer.ManufacturerID
LEFT JOIN   model ON job.ModelID = model.ModelID
LEFT JOIN   unit_type ON model.UnitTypeID = unit_type.UnitTypeID
LEFT JOIN   service_type ON job.ServiceTypeID = service_type.ServiceTypeID
LEFT JOIN   status ON job.StatusID = status.StatusID
LEFT JOIN   service_provider ON job.ServiceProviderID = service_provider.ServiceProviderID
LEFT JOIN   courier ON job.CourierID = courier.CourierID
LEFT JOIN   shipping ON shipping.JobID = job.JobID
LEFT JOIN	branch ON branch.BranchID = job.BranchID

WHERE	    job.DateBooked >= :dateFrom AND job.DateBooked <= :dateTo
