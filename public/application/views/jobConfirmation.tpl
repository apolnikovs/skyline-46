   
<div class="jobConfirmation" id="jobConfirmation" >
        
   
        <form id="jobConfirmationForm" name="jobConfirmationForm" method="post"  action="#" class="inline">
       
           <fieldset>
            <legend title="GP7503" > {$page['Text']['legend']|escape:'html'} </legend>
                 <p>
                    <h2>{$page['Text']['booking_confirmation_check']|escape:'html'}</h2>
                 </p>
            
                <label class="topText"  ><b>{$page['Text']['info_top_text']|escape:'html'}:</b></label>
                <p>
                    <label class="fieldLabel" >{$page['Labels']['customer']|escape:'html'}:</label>
                    <label class="fieldValue" >{$jb_title_name|escape:'html'}&nbsp;{$jb_confirm.ContactFirstName|escape:'html'}&nbsp;{$jb_confirm.ContactLastName|escape:'html'}</label>
                </p>
                <p>
                    <label class="fieldLabel" >{$page['Labels']['email']|escape:'html'}:</label>
                    <label class="fieldValue" id="jcEmailElement" >
                        {if $jb_confirm.ContactEmail neq ''}
                        
                         {$jb_confirm.ContactEmail|escape:'html'}
                         
                         {if $BrandEmailSendFlag}
                         <br>
                         <span class="fieldValueSub" >{$page['Text']['email']|escape:'html'}</span>
                         {/if}
                         
                        {else}
                          
                          {if $BrandEmailSendFlag}   
                            <span class="fieldValueSub" style="color:red;" id="noEmailText" >
                                {$page['Text']['no_email_text1']|escape:'html'} <br>
                                <a href="#" id="email_link" style="text-decoration:underline;" >{$page['Text']['no_email_text2']|escape:'html'}</a> {$page['Text']['no_email_text3']|escape:'html'}
                            </span>
                            <span class="fieldValueSub" id="noEmailTextBox" style="display:none;" >
                            <input  type="text" name="jcContactEmail" class="text"  value="" id="jcContactEmail" >
                            &nbsp;<a href="#" id="save_address_link" style="color:green;text-decoration:underline;" >{$page['Buttons']['save_address']|escape:'html'}</a>
                             </span>
                             <label class="fieldError" id="noEmailTextBoxError" style="display:none;"  >{$page['Errors']['valid_email']|escape:'html'}</label>
                           {/if} 
                           
                       {/if}    
                       
                    </label>
                </p>
               
                {if $jb_confirm.ContactMobile neq ''}

                 <p>
                    <label class="fieldLabel" >{$page['Labels']['mobile']|escape:'html'}:</label>
                    <label class="fieldValue" >    

                                {$jb_confirm.ContactMobile|escape:'html'}

                           {if $jb_confirm.ContactEmail eq ''  && $jb_confirm.ContactMobile neq ''}     
                             &nbsp;<br>
                            <span class="fieldValueSub" >{$page['Text']['mobile']|escape:'html'}</span>
                           {/if}
                    </label>
                 </p>    

                {/if}
                        
                       
              {*
                <p>
                    <label class="fieldLabel" >{$page['Labels']['job_type']|escape:'html'}:</label>
                    <label class="fieldValue" >{$jb_job_type_name|escape:'html'}&nbsp;
                    </label>
                </p>*}
                
                <p>
                    <label class="fieldLabel" >{$page['Labels']['product_type']|escape:'html'}:</label>
                    <label class="fieldValue" >{$jb_product_type_name|escape:'html'}&nbsp;
                    </label>
                </p>
                
                <p>
                    <label class="fieldLabel" >{$page['Labels']['product_location']|escape:'html'}:</label>
                    <label class="fieldValue" id="product_location_label" >{$product_location_name|escape:'html'}&nbsp;
                    </label>
                </p>
                
                {if $displaySPChangeLink eq true}
                    <p>
                        <label class="fieldLabel" >{$page['Labels']['service_provider']|escape:'html'}:</label>
                        <label class="fieldValue" >
                        <span style="padding:0px;" id="ServiceProvideName" >

                        {$jb_service_provider_details|escape:'html'}    

                            </span>
                            &nbsp;&nbsp;  
                           
                            <a href="#" id="ServiceProvideChangeLink" class="moreInfo" >{$page['Buttons']['change']|escape:'html'}</a> / 
                            <a href="#" id="ServiceProvideDefaultLink" class="moreInfo" >{$page['Buttons']['default']|escape:'html'}</a>

                        </label>

                    </p>
                {/if} 
                <p>
                    <label class="fieldLabel" >{$page['Labels']['service_type']|escape:'html'}:</label>
                    <label class="fieldValue" >{$jb_service_type_name|escape:'html'}&nbsp;&nbsp;<br><br>
                    </label>
                </p>
                
                <p>
                    <label class="fieldLabel" >{$page['Labels']['data_protection_act']|escape:'html'}:</label>
                    <label class="fieldValue" >
                        <input style="padding-left:0px;margin-left:0px;" type="radio" name="data_protection_act" id="data_protection_act_yes"  value="1" {if $jb_confirm.jb_data_protection_act=="1"} checked="checked"  {/if}  /> {$page['Labels']['radio_yes']|escape:'html'} &nbsp;  
                        <input  type="radio" name="data_protection_act" id="data_protection_act_no" value="0" {if $jb_confirm.jb_data_protection_act=="0"} checked="checked" {/if}  /> {$page['Labels']['radio_no']|escape:'html'} &nbsp; 
                        
                        <a href="#" class="moreInfo" id="data_protection_act_moreinfo" >{$page['Text']['more_info']|escape:'html'}</a>
                        
                        <br>
                        <span class="fieldValueSub" style="padding-left:0px;margin-left:0px;" >{$page['Text']['data_protection_act']|escape:'html'}&nbsp;&nbsp;
                        
                        </span>
                     </label>    
                </p>
              
                <p>
                    
                     <span class= "bottomButtons" >
                        
                         <input type="submit" name="submit_booking_btn" class="form-button centerBtn" id="submit_booking_btn"  value="{$page['Buttons']['confirm_details']|escape:'html'}" >
                       
                         <span id="processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Buttons']['process_record']|escape:'html'}</span>
                    </span>
                     
                </p>
                
                <p>
                    
                     <span class= "bottomButtons" >
                      
                         <input type="submit" name="edit_booking_btn" class="textSubmitButton leftBtn" id="edit_booking_btn"  value="{$page['Buttons']['edit_booking_details']|escape:'html'}" >
                             
                        <input type="submit" name="cancel_booking_btn" class="textSubmitButton rightBtn" id="cancel_booking_btn"  value="{$page['Buttons']['cancel_job_booking']|escape:'html'}" >
                        
                      </span>
                     
                </p>
       
                 {foreach $jb_confirm as $jIndex => $jValue}            
                    <input  type="hidden" name="{$jIndex|escape:'html'}" value="{$jValue|escape:'html'}"   />  
                 {/foreach}  
                  
                 <input type="hidden" name="ServiceProviderID" id="ServiceProviderID" value="{$ServiceProviderID|escape:'html'}"   /> 

                 <input type="hidden" name="DefaultServiceProviderID" id="DefaultServiceProviderID" value="{$ServiceProviderID|escape:'html'}"   /> 
                 <input type="hidden" name="DefaultServiceProvideName" id="DefaultServiceProvideName" value=" {$jb_service_provider_details|escape:'html'}"   /> 
                 <input type="hidden" name="default_product_location" id="default_product_location" value="{$product_location_name|escape:'html'}"   />

                 
                 <input  type="hidden" name="contact_method_mail" id="contact_method_mail" value="1"   />  
                 <input  type="hidden" name="contact_method_letter" id="contact_method_letter" value="1"   />  
                 <input  type="hidden" name="contact_method_sms" id="contact_method_sms" value="1"   />  
                
             </fieldset>
        
        </form>
                               
</div>

                    
                    
<div  id="jobCancelationRequest" class="jobCancelationRequest" style="display:none;" >
    
        <form id="jobCancelationForm" name="jobCancelationForm" method="post"  action="#" class="inline" >
       
           <fieldset>
            <legend title="GP7504" > {$page['Text']['jcr_legend']|escape:'html'} </legend>
            
            <label class="topText" > {$page['Text']['jcr_info_top_text']|escape:'html'}<br><br> </label>
           
            <p>
                    <span class="bottomButtons" >
                        
                          <input type="submit" name="abort_cancelation_btn" class="textSubmitButton" id="abort_cancelation_btn"  value="{$page['Buttons']['abort_cancelation']|escape:'html'}" >
                        
                          <br><br>
                          
                           <input type="submit" name="proceed_cancellation_btn" class="textSubmitButton" id="proceed_cancellation_btn"  value="{$page['Buttons']['proceed_cancellation']|escape:'html'}" >
                         
                     </span>
            </p>
            
            </fieldset>
            
        </form>
   
</div>    
                 
                 
                 
<div  id="dataProtectionPage" class="dataProtectionPage" style="display:none;" >
    
        <form id="dataProtectionForm" name="dataProtectionForm" method="post"  action="#" class="inline" >
       
           <fieldset>
            <legend title="GP7508" > {$page['Text']['dp_legend']|escape:'html'} </legend>
            
            
            <p>
                    <h2>
                        {$page['Text']['customer_assurance_policy']|escape:'html'}
                    </h2>
            </p>
            
            <p > 
            
            
                
                {$page['Text']['cap_text1']|escape:'html'}:<br>
                <ul class="leftText" >
                    <li>{$page['Text']['cap_text_point1']|escape:'html'}</li>
                    <li>{$page['Text']['cap_text_point2']|escape:'html'}</li>  
                    <li>{$page['Text']['cap_text_point3']|escape:'html'}</li>
                    <li>{$page['Text']['cap_text_point4']|escape:'html'}</li> 
                    <li>{$page['Text']['cap_text_point5']|escape:'html'}</li> 

                </ul>
              
            
            <table>
                
                <tr>
                    <th class="firstColumn" >{$page['Text']['question']|escape:'html'}</th>
                    <th>{$page['Text']['answer']|escape:'html'}</th>
                </tr>
                
                
                <tr>
                    <td class="firstColumn" >{$page['Text']['question1']|escape:'html'}</td>
                    <td>{$page['Text']['answer1']|escape:'html'}</td>
                </tr>
                
                {if $ShowCRMEmailTextFlag}
                <tr>
                    <td class="firstColumn" >{$page['Text']['question2']|escape:'html'}</td>
                    <td>
                        {$page['Text']['answer2_1']|escape:'html'} <a href="#" style="text-decoration:underline;" >{$page['Text']['answer2_2']|escape:'html'}</a> {$page['Text']['answer2_3']|escape:'html'}  
                    </td>
                </tr>
                {/if}
                
                <tr>
                    <td class="firstColumn" >{$page['Text']['question3']|escape:'html'}</td>
                    <td>
                    {$page['Text']['answer3']|escape:'html'} 
                    </td>
                </tr>
                
                 <tr>
                    <td class="firstColumn" >{$page['Text']['question4']|escape:'html'}</td>
                    <td>{$page['Text']['answer4']|escape:'html'}</td>
                </tr>
                
                 <tr>
                    <td class="firstColumn" >{$page['Text']['question5']|escape:'html'}</td>
                    <td>{$page['Text']['answer5']|escape:'html'} </td>
                </tr>
                
                
            </table>    
            
             <div   >
            <span>
                {$page['Text']['future_contact']|escape:'html'}
                <br>
                <input  type="radio" name="data_protection_act" id="data_protection_act_1" checked="checked"  value="1"  /> {$page['Labels']['radio_yes']|escape:'html'} <input  type="radio" name="data_protection_act"  value="0"  /> {$page['Labels']['radio_no']|escape:'html'}

                    <span class="rightText" >{$page['Labels']['contact_method']|escape:'html'}:  &nbsp;{$page['Labels']['email']|escape:'html'} <input  type="checkbox" id="contact_method_1" name="contact_method"  value="Email" checked />&nbsp;&nbsp;{$page['Labels']['letter']|escape:'html'} <input  type="checkbox" name="contact_method" id="contact_method_2"  value="Letter" checked /> 
                    &nbsp;&nbsp;{$page['Labels']['sms']|escape:'html'} <input  type="checkbox" name="contact_method" id="contact_method_3"  value="SMS" checked  />
                    </span>
            </span>
                
            </div>     
            
            </p>
            <p>
                    
                    <span class="bottomButtons" >
                           <input type="submit" name="data_protection_finish_btn" class="textSubmitButton" id="data_protection_finish_btn"  value="{$page['Buttons']['finish']|escape:'html'}" >
                    </span>
            </p>
            
            </fieldset>
            
        </form>
   
</div> 
 
                    
<script type="text/javascript" >                    
   $(document).ready(function() {                 
    $('#ServiceProvidersResults'+"{$tableRandNumber}").PCCSDataTable( {

            displayButtons: "P",
            "aoColumns": [ 

                    /* ServiceProviderID */  null,
                    /* Service Provider Name */  null,
                    /* Town */ null

            ],
            searchCloseImage:   '',
            htmlTablePageId:    'ServiceProvidersPage',
            htmlTableId:        'ServiceProvidersResults'+"{$tableRandNumber}",
            pickButtonId:        'pickButtonId_sp',
            pickCallbackMethod: 'assignServiceProvider',
            dblclickCallbackMethod: 'assignServiceProvider',
            tooltipTitle: "{$page['Text']['tooltip_title']|escape:'html'}",
            colorboxForceClose: false,
            formViewButton:     'formViewButtonSP',
            colorboxFormId:     'colorboxFormIdSP',
            fetchDataUrl:       '{$_subdomain}/Data/getServiceProvidersDTList/'+urlencode("{$jb_confirm.NetworkID|escape:'html'}")+"/1/1"+"/"+ Math.random(),
            popUpFormWidth:     0, //Put 0 for default width
            popUpFormHeight:    0, //Put 0 for default height
            frmErrorRules: {                },
            frmErrorMessages : { },
            sDom: 'ft<"#dataTables_command'+"{$tableRandNumber}"+'">rp',
            bottomButtonsDivId:'dataTables_command'+"{$tableRandNumber}"

        }); 
        
        {if $ServiceProviderID eq '' || $ServiceProviderID eq false}
         $('#ServiceProvideName').css({ "color":"red", "font-size":"11px" });     
        {/if}
        
        });
 </script>       
                    
 <div  id="ServiceProvidersPage" class="ServiceProvidersPage" style="display:none;"  >   
     
        <form id="ServiceProvidersForm" name="ServiceProvidersForm" method="post"  action="#" class="inline" >
       
           <fieldset>
            <legend> {$page['Text']['service_provider_legend']|escape:'html'} </legend>
            
                 <table id="ServiceProvidersResults{$tableRandNumber}" border="0" cellpadding="0" cellspacing="0" class="browse">
                <thead>
                        <tr>
                                <th width="10" >{$page['Text']['id']|escape:'html'}</th>
                                <th width="60%" >{$page['Text']['service_provider_name']|escape:'html'}</th>
                                <th width="30%" >{$page['Text']['town']|escape:'html'}</th>
                        </tr>
                </thead>
                <tbody>

                </tbody>
                </table>  
            
            
            </fieldset>
            
        </form>
                        
        <div class="bottomButtonsPanel" >
            <hr>

            <button id="sp_dt_finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
        </div>                 
 </div>    
                    