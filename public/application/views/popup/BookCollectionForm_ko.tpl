{extends "./TestLayout.tpl"}

 {block name=body}
 <style>
     #BookCollectionForm { width: 600px; }
     #BookCollectionForm label { width: 200px; }
     #BookCollectionForm label sup { font-weight:bold; position: absolute; right: -0.6em; top: 0.6em; }
     #EditButton { text-decoration: underline;
                   font-size: 1.2em;
                   font-weight: bold; }
     #CollectionDate { width: 276px; }
     #Postcode { width: 146px; }
     #BookCollectionForm .form_buttons_container { width: 532px; display: inline-block; text-align: right; }
     #quick_find_btn { background-position: 0px 0px; }
     #quick_find_btn:hover { background-position: 0px -21px; }
</style>

<script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
<script type='text/javascript' src='{$_subdomain}/js/knockout-2.2.1.js'></script>

<script type="text/javascript">
 
var Courier = function(name, id) {
        this.courierName = name;
        this.courierID = id;    
    };
var Country = function(name, id) {
        this.countryName = name;
        this.countryID = id;    
    };  
    
var County = function(name, id) {
        this.countyName = name;
        this.countyID = id;    
    };
    
// This is a *viewmodel* - JavaScript that defines the data and behavior of the UI
function AppViewModel() {
    this.Direction = "inbound";
    this.CollectionDate = ko.observable("");
    this.PreferredCourier = ko.observableArray([]);
    this.ConsignmentNo = ko.observable("");
    this.Name = ko.observable("");
    this.Postcode = ko.observable("");
    this.AddressSelect = ko.observable("");
    this.BuildingName = ko.observable("");
    this.Street = ko.observable("");
    this.Area = ko.observable("");
    this.Town = ko.observable("");
    this.County = ko.observableArray([]);
    this.Country = ko.observableArray([]);
    this.Email = ko.observable("");
    this.Phone = ko.observable("");
    this.CollectionLegend = ko.observable("{$page['Labels']['collection_legend']|escape:'html'}");
    this.CollectionDateLabel = ko.observable("{$page['Labels']['collection_date']|escape:'html'}");
    this.EditButton = ko.observable("{$page['Buttons']['edit_collection_address']|escape:'html'}");
}

$(document).ready(function() {

    // Activates knockout.js
    ko.applyBindings(new AppViewModel());

    $( '#CollectionDate' ).datepicker({
            dateFormat: 'dd/mm/yy',
            showOn: 'button',
            buttonImage: '{$_subdomain}/css/Skins/{$_theme}/images/calendar.png',
            buttonImageOnly: true,
            maxDate: '0',
            changeMonth: true,
            changeYear: true
      });
      
    $("input[name='direction']").change(function() {
        if ($('input:radio[name=direction]:checked').val() == 'inbound') {
            $('#CollectionLegend').html("{$page['Labels']['collection_legend']|escape:'html'}");
            $('#CollectionDateLabel').html("{$page['Labels']['collection_date']|escape:'html'}");
            $('#EditButton').html("{$page['Buttons']['edit_collection_address']|escape:'html'}");
        } else {
            $('#CollectionLegend').html("{$page['Labels']['delivery_legend']|escape:'html'}");
            $('#CollectionDateLabel').html("{$page['Labels']['delivery_date']|escape:'html'}");
            $('#EditButton').html("{$page['Buttons']['edit_delivery_address']|escape:'html'}");
        }
      });
      
    $('#EditButton').click(function() {
            $('#CollectionAddressPanel').toggle();
            $.colorbox.resize();      
      });
      
    $('#CancelButton').click(function() {
            $.colorbox.close();    
      })
      
    /* Update counties when county changes */
    $('#Country').change(function() {
            document.body.style.cursor = 'wait';
            $.ajax({ type: 'POST',
                 dataType: 'html',
                 data: 'CountryID=' + $(this).val(),
                 success: function(data, textStatus) {
                    $('#selectCounty').html(data);
                 },
                 complete: function(XMLHttpRequest, textStatus) {
                    document.body.style.cursor = 'default';
                 },
                 url: '{$_subdomain}/Courier/CountyLookup'

            });

            return false;
    
    });
      
    /* Check the postcode */
    $('#quick_find_btn').click(function() {

        $.ajax({ type: 'POST',
                 dataType: 'html',
                 data: 'Postcode=' + $('#Postcode').val(),
                 success: function(data, textStatus) {
                    $("#selectOutput").html(data).slideDown("slow");
                 },
                 beforeSend: function(XMLHttpRequest) {
                    $('#fetch').fadeIn('medium');
                 },
                 complete: function(XMLHttpRequest, textStatus) {
                    $('#fetch').fadeOut('medium');
                    $('#postSelect').change(function() {
                        var selected_split = $(this).val().split(",");
                                               
                        $("#BuildingName").val(trim(selected_split[6])).blur();	
                        $("#Street").val(trim(selected_split[1])).blur();
                        $("#Area").val(trim(selected_split[2])).blur();
                        $("#Town").val(trim(selected_split[3])).blur();
                
                        $("#County").val(trim(selected_split[4])).blur();
                        setValue("#County");
                
                        $("#Country").val(trim(selected_split[5])).blur();
                        setValue("#Country");
                
                        $("#selectOutput").slideUp("slow");
                    });
                 },
                 url: '{$_subdomain}/Courier/PostcodeLookup'

            });

            return false;

        });
      
});
</script>
     
<form id="BookCollectionForm" name="BookCollectionForm" action="{$_subdomain}/Courier/BookCollectionForm" class="inline" method="post" >
    <fieldset>
        <legend id="CollectionLegend" title="" date-bind="value: CollectionLegend" />
        <input type="hidden" name="JobID" value="{$job_id}" />
        <p>
            <label >&nbsp;</label>
            <input type="radio" name="Direction" value="inbound" checked >{$page['Labels']['inbound']|escape:'html'}</input>
            <input type="radio" name="Direction" value="outbound">{$page['Labels']['outbound']|escape:'html'}</input>
        </p>
        <p>
            <label id="CollectionDateLabel" for="CollectionDate">{$page['Labels']['collection_date']|escape:'html'}:<sup>*</sup></label>
            <input type="text" id="CollectionDate" name="CollectionDate" class="text" required="required" date-bind="value: CollectionDate" />
        </p>
        <p>
            <label for="PreferredCourier">{$page['Labels']['preferred_courier']|escape:'html'}:</label>
            <select name="PreferredCourier" id="PreferredCourier" class="text" date-bind="value: PreferredCourier"  >
                <option value="" selected="selected">{$page['Text']['please_select']|escape:'html'}</option>
		{foreach $couriers as $courier}
                <option value="{$courier.CourierID}" >{$courier.CourierName|escape:'html'}</option>
		{/foreach}
            </select>
        </p>
        <p>
            <label for="ConsignmentNo">{$page['Labels']['consignment_no']|escape:'html'}:</label>
            <input type="text" id="ConsignmentNo" name="ConsignmentNo" class="text" date-bind="value: ConsignmentNo" />
        </p>
        <p>
            <br />
            <label><a id="EditButton" href="#" date-bind="value: EditButton" ></a></label>
            <br />&nbsp;<br />
        </p>
        <div id="CollectionAddressPanel" style="display: none;">
            <p>
                <label for="Name">{$page['Labels']['name']|escape:'html'}:</label>
                <input type="text" id="Name" name="Name" class="text" date-bind="value: Name" />
            </p>  
            <p>
                <label for="Postcode">{$page['Labels']['postcode']|escape:'html'}:<sup>*</sup></label>
                <input type="text" id="Postcode" name="Postcode" class="text" required="required" date-bind="value: Postcode" />
                <input type="button" name="quick_find_btn" id="quick_find_btn" style="" class="postCodeLookUpBtn"  value="" />
                <img id="fetch" src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" style="position: relative; top: 4px; display: none;" />
            </p>
            <p id="selectOutput" style="display: none;"></p>
            <p>
                <label for="BuildingName">{$page['Labels']['building_name']|escape:'html'}:</label>
                <input type="text" id="BuildingName" name="BuildingName" class="text" date-bind="value: BuildingName" />
            </p>
            <p>
                <label for="Street">{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>
                <input type="text" id="Street" name="Street" class="text" required="required" date-bind="value: Street" />
            </p>
            <p>
                <label for="Area">{$page['Labels']['area']|escape:'html'}:</label>
                <input type="text" id="Area" name="Area" class="text" date-bind="value: Area" />
            </p>
            <p>
                <label for=Town">{$page['Labels']['town']|escape:'html'}:</label>
                <input type="text" id="Town" name="Town" class="text" date-bind="value: Town" />
            </p>
            <p>
                <label for="County">{$page['Labels']['county']|escape:'html'}:</label>
                <span id="selectCounty">
                <select name="County" id="County" class="text" date-bind="value: County" >
                    <option value="" selected="selected">{$page['Text']['please_select']|escape:'html'}</option>
                    {foreach $counties as $county}
                    <option value="{$county.CountyID}" >{$county.Name|escape:'html'}</option>
                    {/foreach}
                </select>
                </span>
            </p>
            <p>
                <label for="Country">{$page['Labels']['country']|escape:'html'}:<sup>*</sup></label>
                <select name="Country" id="Country" class="text" date-bind="value: Country" >
                    {* <option value="" selected="selected">{$page['Text']['please_select']|escape:'html'}</option> *}
                    {foreach $countries as $country}
                    <option value="{$country.CountryID}" {if $country.Name eq 'UK'}selected{/if}>{$country.Name|escape:'html'}</option>
                    {/foreach}
                </select>
            </p>
            <p>
                <label for="Email">{$page['Labels']['email']|escape:'html'}:</label>
                <input type="text" id="Email" name="Email" class="text" date-bind="value: Email" />
            </p>
            <p>
                <label for="Phone">{$page['Labels']['phone']|escape:'html'}:</label>
                <input type="text" id="Phone" name="Phone" class="text" date-bind="value: Phone" />
            </p>
        </div>
        <p class="form_buttons_container">
            <button type="submit" class="gplus-blue"><span class="label">{$page['Buttons']['save']|escape:'html'}</span></button>
            <button id="CancelButton" type="button" class="gplus-blue"><span class="label">{$page['Buttons']['cancel']|escape:'html'}</span></button>
        </p>
    </fieldset>
</form>

 {/block}