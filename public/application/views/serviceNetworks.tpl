{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $ServiceNetworks}
{/block}

{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
        .ui-combobox-input {
             width:300px;
         }  
    </style>
{/block}
{block name=scripts}

    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}

    <link rel="stylesheet" href="{$_subdomain}/css/tooltipster/tooltipster.css" type="text/css" charset="utf-8" />
    <script type="text/javascript" src="{$_subdomain}/js/jquery.tooltipster.min.js"></script>
    
    <script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery.placeholder.min.js"></script>

    <script type="text/javascript">

    $.validator.addMethod(
	"multiEmail",
	function(value, element) {
	    if(this.optional(element)) { // return true on optional element 
		return true;
	    }
	    var emails = value.split(/[;]+/); // split element by ;
	    valid = true;
	    for(var i in emails) {
		value = emails[i];
		valid = valid && $.validator.methods.email.call(this, $.trim(value), element);
	    }
	    return valid;
	},
	$.validator.messages.email
    );    
    
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
                    
    var vat_rate_array = [];
    {foreach $countries as $country}
        vat_rate_array[{$country.CountryID}] = [];
        {if isset($vatRates[$country.CountryID])}            
            {foreach $vatRates[$country.CountryID] as $vatRateItem}
                vat_rate_array[{$country.CountryID}][{$vatRateItem.VatRateID}] = '{$vatRateItem.Code}';
            {/foreach}
        {/if}
    {/foreach}
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    function inactiveRow(nRow, aData)
    {
          
        if (aData[4]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(3)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
                  $('td:eq(3)', nRow).html( $statuses[0][0] );
        }
    }
    
    
 
 
    $(document).ready(function() {

	//init tooltip
	//$(jobsTable.fnGetNodes()).find(".hoverCell").parent().css("padding","0");
	//$(jobsTable.fnGetNodes()).find(".hoverCell").css("padding","7px 9px");
	
	$(".hover").tooltipster({
	    arrow:	    false,
	    delay:	    0,		
	    followMouse:    true,
	    position:	    "top-right",
	    tooltipTheme:   ".tableHover"
	});


	$('input[placeholder], textarea[placeholder]').placeholder();
             
                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/organisationSetup');

                                });


                  
                  $(document).on('click', '#quick_find_btn', 
                                function() {

                    $.ajax( { type:'POST',
                            dataType:'html',
                            data:'postcode=' + $('#PostalCode').val(),
                            success:function(data, textStatus) { 

                                    if(data=='EM1011' || data.indexOf("EM1011")!=-1)
                                    {

                                            $('#selectOutput').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                            $("#selectOutput").slideDown("slow");
                                           

                                            $("#BuildingNameNumber").val('');	
                                            $("#Street").val('');
                                            $("#LocalArea").val('');
                                            $("#TownCity").val('');
                                            $('#PostalCode').focus();

                                    }
                                    else
                                    {

                                        $('#selectOutput').html(data); 
                                        $("#selectOutput").slideDown("slow");
                                        $("#postSelect").focus();

                                    }




                            },
                            beforeSend:function(XMLHttpRequest){ 
                                   // $('#fetch').fadeIn('medium'); 
                            },
                            complete:function(XMLHttpRequest, textStatus) { 
                                   // $('#fetch').fadeOut('medium') 
                                    setTimeout("$('#quickButton').fadeIn('medium')");
                            },
                            url:'{$_subdomain}/index/addresslookup' 
                        } ); 
                    return false;
                    
                    
                });                 



                 /* Add a change handler to the county dropdown - strats here*/
                /*$(document).on('change', '#CountyID', 
                    function() {
                         
                         
                         $("#CountyID option:selected").each(function () {
                            $selected_cc_id =  $(this).attr('id');
                            $country_id_array = $selected_cc_id.split('_');
                            if($country_id_array[1]!='0')
                            {
                                $("#CountryID").val($country_id_array[1]);
                                $("#CountryID").blur();
                                $("#CountryID").attr("disabled","disabled").addClass('disabledField');
                            }
                            else
                            {
                                 $("#CountryID").val('');
                                 $("#CountryID").removeAttr("disabled").removeClass('disabledField');
                            }
                            
                            
                        });

                        
                        
                    }      
                );*/
              /* Add a change handler to the county dropdown - ends here*/
          
          
             /***************************************
             * Change handler for Country drop down *
             ***************************************/
             /*$(document).on('change', '#CountryID', 
                    function() {
                        var options = '';
                        var selectedCountryRates = vat_rate_array[$('#CountryID').val()];
                        
                        $('#VATRateID').empty()

                        $('#VATRateID').append('<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>');
                             
                        for (var n = 0; n < selectedCountryRates.length; n++) {
                            if (selectedCountryRates[n] !== undefined) {
                            options += '<option value="' + n + '">' + selectedCountryRates[n] + '</option>';
                            }
                        }
                        
                        $('#VATRateID').append(options);
                    }      
                );*/
             

        


               
               
                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                     {if $SupderAdmin eq true} 

                        var  displayButtons = "UA";
                     
                     {else}

                         var displayButtons =  "U";

                     {/if} 
                     


                    
		    $('#SNResults').PCCSDataTable( {
			aoColumns: [ 
			    /* NetworkID */	{ bVisible: false },    
			    /* CompanyName */   null,
			    /* ASCReport */	null,
			    /* NetworkReport */	null,
			    /* CreditLevel */	null,
			    /* ReorderLevel */  null,
			    /* Status */	null
			],
			aaSorting: [[ 1, "asc" ]],
			displayButtons:  displayButtons,
			addButtonId:     'addButtonId',
			addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
			createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
			createAppUrl:    '{$_subdomain}/OrganisationSetup/serviceNetworks/insert/',
			createDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/ServiceNetworks/',
			createFormFocusElementId:'CompanyName',
			formInsertButton:'insert_save_btn',
			copyInsertRowId: 'copyRowId',
                            
                            frmErrorRules:   {
                                                    
                                                    CompanyName:
                                                        {
                                                            required: true
                                                        },
                                                    PostalCode:
                                                        {
                                                            required: true
                                                        },
                                                    Street:
                                                        {
                                                            required: true
                                                        },
                                                    TownCity:
                                                        {
                                                            required: true
                                                        },
                                                    country:
                                                        {
                                                            required: true
                                                        },
                                                    ContactEmail:
                                                        {
                                                            required: true,
                                                            email:true
                                                        }, 
                                                    CreditPrice:
                                                        {
                                                            required: true,
                                                            number: true
                                                        },  
                                                    CreditLevel:
                                                        {
                                                            required: true,
                                                            digits: true
                                                        },
                                                    ReorderLevel:
                                                        {
                                                            required: true,
                                                            digits: true
                                                        },
                                                    SageReferralNominalCode:
                                                        {
                                                            digits: true
                                                        },
                                                    SageLabourNominalCode:
                                                        {
                                                            digits: true
                                                        },
                                                    SagePartsNominalCode:
                                                        {
                                                            digits: true
                                                        },
                                                    SageCarriageNominalCode:
                                                        {
                                                            digits: true
                                                        },
						    ServiceManagerEmail: { email: true },
						    AdminSupervisorEmail: { email: true },
						    NetworkManagerEmail: { multiEmail: true }
                                                        
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    CompanyName:
                                                        {
                                                            required: "{$page['Errors']['company_name']|escape:'html'}"
                                                        },
                                                    PostalCode:
                                                        {
                                                            required: "{$page['Errors']['post_code']|escape:'html'}"
                                                        },
                                                    Street:
                                                        {
                                                            required: "{$page['Errors']['street']|escape:'html'}"
                                                        },
                                                    TownCity:
                                                        {
                                                            required: "{$page['Errors']['town']|escape:'html'}"
                                                        },
                                                    country:
                                                        {
                                                            required: "{$page['Errors']['country']|escape:'html'}"
                                                        },
                                                    ContactEmail:
                                                        {
                                                            required: "{$page['Errors']['email']|escape:'html'}",
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"
                                                        },  
                                                    CreditPrice:
                                                        {
                                                            required: "{$page['Errors']['credit_price']|escape:'html'}",
                                                            number: "{$page['Errors']['valid_credit_price']|escape:'html'}"
                                                            
                                                        },
                                                    CreditLevel:
                                                        {
                                                            required: "{$page['Errors']['credit_level']|escape:'html'}",
                                                            digits: "{$page['Errors']['valid_credit_level']|escape:'html'}"
                                                        },
                                                    ReorderLevel:
                                                        {
                                                            required: "{$page['Errors']['reorder_level']|escape:'html'}",
                                                            digits: "{$page['Errors']['valid_reorder_level']|escape:'html'}"
                                                        },
                                                    SageReferralNominalCode:
                                                        {
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
                                                    SageLabourNominalCode:
                                                        {
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
                                                    SagePartsNominalCode:
                                                        {
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
                                                    SageCarriageNominalCode:
                                                        {
                                                            digits: "{$page['Errors']['digits']|escape:'html'}"
                                                        },
						    ServiceManagerEmail: { email: "{$page['Errors']['valid_email']|escape:'html'}" },
						    AdminSupervisorEmail: { email: "{$page['Errors']['valid_email']|escape:'html'}" },
						    NetworkManagerEmail: { 
							//email: "{$page['Errors']['valid_email']|escape:'html'}",
							multiEmail: "Must be valid multiple emails separated by ;"
						    }
					
                                              },                     
                            
                            popUpFormWidth:  750,
                            popUpFormHeight: 430,
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/OrganisationSetup/serviceNetworks/update/',
                            updateDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/ServiceNetworks/',
                            formUpdateButton:'update_save_btn',
                            updateFormFocusElementId: 'CompanyName',
                            
                            colorboxFormId:  "SNForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'SNResultsPanel',
                            htmlTableId:     'SNResults',
                            fetchDataUrl:    '{$_subdomain}/OrganisationSetup/ProcessData/ServiceNetworks/fetch/',
                            formCancelButton:'cancel_btn',
			    //pickCallbackMethod: 'openJob',
                            dblclickCallbackMethod: 'gotoEditPage',
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError"


                        });
                      



                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/organisationSetup" >{$page['Text']['organisation_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="serviceNetworksForm" name="serviceNetworksForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="SNResultsPanel" >

                    <table id="SNResults" border="0" cellpadding="0" cellspacing="0" class="browse">
                        <thead>
			    <tr>
				<th></th> 
				<th width="30%" title="{$page['Text']['name']|escape:'html'}">
				    {$page['Text']['name']|escape:'html'}
				</th>
				<th width="10%" class="hover"  title="Email Individual ASC's the Daily Network Open Jobs Report">
				    {$page['Text']['asc_report']|escape:'html'}
				</th>
				<th width="10%" class="hover"  title="Email the Network the Daily ASC Open Jobs Report">
				    {$page['Text']['network_report']|escape:'html'}
				</th>
				<th width="20%" title="{$page['Text']['current_credit_level']|escape:'html'}">
				    {$page['Text']['current_credit_level']|escape:'html'}
				</th>
				<th width="20%" title="{$page['Text']['reorder_level']|escape:'html'}">
				    {$page['Text']['reorder_level']|escape:'html'}
				</th>
				<th width="10%" title="{$page['Text']['status']|escape:'html'}">
				    {$page['Text']['status']|escape:'html'}
				</th>
			    </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>  
				
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


                <input type="hidden" name="copyRowId" id="copyRowId" > 

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                        
                        



{/block}



