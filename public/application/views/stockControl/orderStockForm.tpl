<script>
    $(document).ready(function(){
   
    });//end doc ready
    
     
      function calculateCost(f){
    var exRate=$('#exRate').val();
      spC=$('#spCurrency').val();
      supC=$('#supCurrency').val();
      qty=$('#qtyReq').val();
      if(qty==""){
      qty=0;
      }
      
        if(f==1){
           $('#supCurrency').val((spC * exRate).toFixed(2))
        }
        if(f==2){
            $('#spCurrency').val((supC / exRate).toFixed(2))
        }
          spC=$('#spCurrency').val();
      supC=$('#supCurrency').val();
        $('#spCurrencyTot').html((spC * qty).toFixed(2));
        $('#supCurrencyTot').html((supC * qty).toFixed(2));
      }
      
    function getSupCurrencys(){
      $.post("{$_subdomain}/StockControl/getSupCurrencyData",{ sup:$('#ServiceProviderSupplier').val(),spid:{$data['ServiceProviderID']} },
function(data) {
   en=jQuery.parseJSON(data);
  
   $('#supCurrencyTotS').html(en.code);
   $('#supCurrencyS').html(en.code);
   $('#exRate').val(en.rate)
   calculateCost(1);
  
});
    }
</script>
<style>
.cardLabel{
    padding-top:0px;
    }
</style>



<div class="SystemAdminFormPanel" style="width:800px">
    <fieldset>
        <legend>Order Stock</legend>
        <form method=post action="{$_subdomain}/StockControl/createPendingItems">
            <input type="hidden" id="exRate" value="{$currencyRate|default:1}">
            <input type="hidden" name="stockNumber" id="stockNumber" value="{$data.PartNumber|default:''}">
            <input type="hidden" name="SpPartStockTemplateID" id="SpPartStockTemplateID" value="{$data.SpPartStockTemplateID|default:''}">
          <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

          </p>
        <p>
                           <label  class="cardLabel" for="PartNumber" >Stock Code:</label>
                           
                           <span  class="cardSpan" >{$data.PartNumber|default:''}</span>
       </p>
        <p>
                           <label  class="cardLabel" for="Description" >Stock Description:</label>
                           
                           {$data.Description|default:''}
       </p>
       <p>
           <label class="cardLabel" for="ServiceProviderSupplier">Supplier:<sup>*</sup></label>
                          
           <select onchange="getSupCurrencys()" required="required" name="ServiceProviderSupplier" id="ServiceProviderSupplier"  >
                               
                                    <optgroup label="Primary Supplier">
                                    <option value="{$data.DefaultServiceProviderSupplierID}">{$data.CompanyName|default:''}</option>
                                    </optgroup>
                                     <optgroup label="Associated Suppliers">
                                {foreach $assocSuppliers|default:"" as $s}

                                    <option value="{$s.ServiceProviderSupplierID|default:""}" >{$s.CompanyName|default:""}</option>

                                {/foreach}
                                 </optgroup>
                                     <optgroup label="All Suppliers">
                                {foreach $Suppliers|default:"" as $s}

                                    <option value="{$s.ServiceProviderSupplierID|default:""}" >{$s.CompanyName|default:""}</option>

                                {/foreach}
                                 </optgroup>
                                
                            </select>
      </p>
      <p>
                           <label  class="cardLabel" for="Description" >Qty Required:<sup>*</sup></label>
                           
                           <input   onchange="calculateCost(1)" style="width:100px" value="{$qty|default:''}" class="text" required="required" type="text" name="qtyReq" id="qtyReq">
      </p>
      <p>
      <span style="position:relative;left:210px;">Local Currency</span>
      <span style="position:relative;left:234px;">Supplier Currency</span>
      </p>
      <p>
                        <label  class="cardLabel" for="Description" >Item Purchase Price:</label>
                        <input id="spCurrency"  onchange="calculateCost(1);" style="width:50px;" name="PurchaseCost" value="{$data.PurchaseCost}">
                        <span id="spCurrencyS">{$spCurrCode}</span>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <input id="supCurrency"  onchange="calculateCost(2);" style="width:50px;" name="PurchaseCostSupplier" value="{$data.PurchaseCost*$currencyRate|string_format:"%.2f"}">
                        <span id="supCurrencyS">{$supCurrCode}</span>
                            
                            
      </p>
      <p>
                        <label  class="cardLabel" for="Description" >Total Purchase Price:</label>
                        <span id="spCurrencyTot">0</span>
                        <span id="spCurrencyTotS">{$spCurrCode}</span>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        
                        <span style="margin-left:53px;" id="supCurrencyTot">0</span>
                        <span id="supCurrencyTotS">{$supCurrCode}</span>
                            
                            
      </p>
      <hr>
                               
      {if $mode==1}
                                <div style="height:20px;margin-bottom: 10px;text-align: center;">
                                <button type="submit" style="margin-left:38px" class="gplus-blue centerBtn">Save</button>
                                <button type="button" onclick="$.colorbox.close();"  class="gplus-blue" style="float:right">Cancel</button>
                                </div>
       {elseif $mode==2}
           
                                <div style="height:20px;margin-bottom: 10px;text-align: center;">
                                <button type="button" onclick="addOrderedParts()" style="margin-left:38px" class="gplus-blue centerBtn">Order</button>
                                <button type="button" onclick="$.colorbox.close();"  class="gplus-blue" style="float:right">Cancel</button>
                                </div>
       {/if}
           
           
           
    </form>
    </fieldset>

</div>