{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Accessory}
    {$fullscreen=true}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
        .ui-combobox-input {
            width:300px;
        }
    </style>
{/block}

{block name=scripts}



    <script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
    <script type="text/javascript">
        function showTablePreferences(){
        $.colorbox({

        href:"{$_subdomain}/ProductSetup/tableDisplayPreferenceSetup/page=Accessory/table=Accessory",
                title: "Table Display Preferences",
                opacity: 0.75,
                overlayClose: false,
                escKey: false

        });
       }
var oldValue;
        $(document).ready(function() {
        
  	$('input[id^=inactivetick]').click( function() {
       
        if($(this).attr("checked")=="checked"){
                 $('#tt-Loader').show();
                 $('#AccessoryResults').hide();
               
		 oTable.fnReloadAjax("{$_subdomain}/ProductSetup/loadAccessoryTable/inactive=1/");
                 
                 }else{
                  $('#tt-Loader').show();
                  $('#AccessoryResults').hide();
		 oTable.fnReloadAjax("{$_subdomain}/ProductSetup/loadAccessoryTable/");
                 }
	} );

    var oTable = $('#AccessoryResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',
"bServerSide": true,

		
    "sAjaxSource": "{$_subdomain}/ProductSetup/loadAccessoryTable/{if $showSpUnmodified|default:''===true}showSpUnmodified=show/{/if}",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                                $('#tt-Loader').hide();
                                $('#AccessoryResults').show();
                               
			} );
                        },
                        "oLanguage": {
                                "sLengthMenu": "_MENU_ Records per page",
                                "sSearch": "Search within results"
                            },

            "bPaginate": true,
            "sPaginationType": "full_numbers",
            "aLengthMenu": [[ 10, 15, 25, 50, 100 , -1], [10, 15, 25, 50, 100, "All"]],
            "iDisplayLength" : 10,

            "aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            { "bVisible":{$vis} },
			 
                           {/for} 
                               
                               { "bVisible":1,"bSortable":false }
                            
		] 
        
          
});
        
        
        
        $("#uId").combobox({
        change: function() {
        if($("#uId").val()<0)
            {                    

                    $(location).attr('href', '{$_subdomain}/ProductSetup/accessory/');
                    }
                    else
                        {
        $(location).attr('href', '{$_subdomain}/ProductSetup/accessory/' + urlencode($("#uId").val()));
                            }
        }
        });
        /*        var oTable = $('#AccessoryResults').dataTable({
        "sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',
                "bPaginate": true,
                "sPaginationType": "full_numbers",
                "aLengthMenu": [[ 25, 50, 100 ], [25, 50, 100]],
                "iDisplayLength" : 25,
                



                                            }); */ //datatable end
                                    

                                    
                                    
                   /*       $('#tt-Loader').hide();
                            $('#AccessoryResults').show();   */  
                                    
                                          
                                            
                                            /* Add a click handler to the rows - this could be used as a callback */
                                            $("#AccessoryResults tbody").click(function(event) {
                                    $(oTable.fnSettings().aoData).each(function (){
                                    $(this.nTr).removeClass('row_selected');
                                    });
                                            $(event.target.parentNode).addClass('row_selected');
                                            var anSelected = fnGetSelected(oTable);
                                            var aData = oTable.fnGetData(anSelected[0]); // get datarow

                                            if (anSelected != "")  // null if we clicked on title row
    {

        $('#exportType').val(aData[0]);
        }
        });
                /* Get the rows which are currently selected */
                        function fnGetSelected(oTableLocal)
                                {
                                var aReturn = new Array();
                                        var aTrs = oTableLocal.fnGetNodes();
                                        for (var i = 0; i < aTrs.length; i++)
        {
                                    if ($(aTrs[i]).hasClass('row_selected'))
                {
                                            aReturn.push(aTrs[i]);
                                            }
                                            }
                                            return aReturn;
                                                    }



                                    /* Add a click handler for the edit row */
                                    $('button[id^=edit]').click(function() {
                                    var anSelected = fnGetSelected(oTable);
                                            var aData = oTable.fnGetData(anSelected[0]); // get datarow
                                            if (anSelected != "")  // null if we clicked on title row
    {
                        //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
                        
                        $.colorbox({

                        href:"{$_subdomain}/ProductSetup/processAccessory/id=" + anSelected[0].id + "/nid={$nId}",
                                title: "Edit Accessory",
                                opacity: 0.75,
                                width:800,
                                overlayClose: false,
                                escKey: false, 
                                onClosed:function(){ oTable.fnDraw(true); } 

                        });
                        } else{
                        alert("Please select row first");
                        }

                        });
                                /* Add a click handler for the delete row */
                                $('button[id^=delete]').click(function() {
                        var anSelected = fnGetSelected(oTable);
                                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                                if (anSelected != "")  // null if we clicked on title row
    {
                        //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
                        if (confirm('Are you sure you want to delete this entry from database?')) {
                        window.location = "{$_subdomain}/ProductSetup/deleteAccessory/id=" + anSelected[0].id
                                } else {
                        // Do nothing!
                        }


                        } else{
                        alert("Please select row first");
                        }

                        });
                                /* Add a dblclick handler to the rows - this could be used as a callback */
                                $("#AccessoryResults  tbody").dblclick(function(event) {
                        $(oTable.fnSettings().aoData).each(function (){
                        $(this.nTr).removeClass('row_selected');
                        });
                                $(event.target.parentNode).addClass('row_selected');
                                var anSelected = fnGetSelected(oTable);
                                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                                if (anSelected != "")  // null if we clicked on title row
    {

                        $.colorbox({

                        href:"{$_subdomain}/ProductSetup/processAccessory/id=" + anSelected[0].id + "/nid={$nId}",
                                title: "Edit Accessory",
                                opacity: 0.75,
                                width:800,
                                overlayClose: false,
                                escKey: false

                        });
                        } else{
                        alert("Please select row first");
                        }

                        });
                        }); //doc ready end
                        function AccessoryInsert()
                                {
                                $.colorbox({

                                href:"{$_subdomain}/ProductSetup/processAccessory/",
                                        title: "Insert Accessory",
                                        opacity: 0.75,
                                        width:800,
                                        overlayClose: false,
                                        escKey: false

                                });
                                        }
                        function AccessoryEdit()
                                {
                                var anSelected = fnGetSelected(oTable);
                                        var aData = oTable.fnGetData(anSelected[0]);
                                        $.colorbox({

                                href:"{$_subdomain}/ProductSetup/processAccessory/id=" + aData,
                                        title: "Edit Accessory",
                                        opacity: 0.75,
                                        width:800,
                                        overlayClose: false,
                                        escKey: false

                                });
                                        }







                        function filterTable(name){
                        $('input[type="text"]', '#AccessoryResults_filter').val(name);
                                e = jQuery.Event("keyup");
                                e.which = 13;
                                $('input[type="text"]', '#AccessoryResults_filter').trigger(e);
                                }

</script>


{/block}


{block name=body}
    <div style="float:right">
        <a href="#" onclick="showTablePreferences();">Display Preferences</a>
    </div>
    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/ProductSetup" >ProductSetup</a> {if isset($backtomodel)}/<a href="{$_subdomain}SystemAdmin/index/ProductSetup/models" > Models</a> {/if}/ Accessory

        </div>
    </div>



    <div class="main" id="home" style="width:100%">

        <div class="ServiceAdminTopPanel" >
            <form id="AccessoryTopForm" name="AccessoryTopForm" method="post"  action="#" class="inline">

                <fieldset>
                    <legend title="" >Accessories</legend>
                    <p>
                        <label>This browse contains a list of accessories in the system, which are normally used in conjunction with mobile phone repairs. For example, a charger is a typical phone accessory.</label>
                    </p> 

                </fieldset> 


            </form>
        </div>  


        <div class="ServiceAdminResultsPanel" id="AccessoryResultsPanel" >




            <div style="text-align:center" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                {$page['Labels']['unit_type_label']|escape:'html'}
            <select name="uId" id="uId" class="topDropDownList" style="float:left">
                <option value="-1" {if $uId eq ''}selected="selected"{/if}>All Unit Types</option>

                {foreach $unitTypes as $unitType}

                    <option value="{$unitType.UnitTypeID}" {if $uId eq $unitType.UnitTypeID}selected="selected"{/if}>{$unitType.UnitTypeName|escape:'html'}</option>

                {/foreach}
            </select>
            <form method="POST" action="{$_subdomain}/ProductSetup/saveAssigned" id="AccessoryResultsForm" class="dataTableCorrections">
           
                    <table id="AccessoryResults" style="display:none" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
			    <tr>
                                {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                  
                                {/foreach}
                                <th style="width:10px"></th>
			    </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>      
                
                
                {* <table style="display:none" id="AccessoryResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                    <thead>
                        <tr>

                            {foreach from=$data_keys key=kk item=vv}

                                <th>
                                    {$vv}
                                </th>

                            {/foreach}
                            {if $nId!=''}
                                <th>Assigned</th>
                                {/if}   
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $data as $d}
                            <tr id="{if isset($data_keys[0])&& isset($d[$data_keys[0]])}{$d[$data_keys[0]]}{/if}">
                                {foreach from=$data_keys key=kk item=vv}
                                    <td>
                                    {if isset($d[$vv])}{$d[$vv]}{/if}
                                </td>
                            {/foreach}
                            {if $nId!='' }
                                <td>
                            {if $cId!=""}
                        {if $d['csig']=="Active"}<input value="{$d['AccessoryID']}" name="assignedMan[]" type="checkbox" checked="checked">{else}<input value="{$d['AccessoryID']}" name="assignedMan[]" type="checkbox">{/if}
                    {else}
                {if $d['asig']=="Active"}<input value="{$d['AccessoryID']}" name="assignedMan[]" type="checkbox" checked="checked">{else}<input name="assignedMan[]" value="{$d['AccessoryID']}" type="checkbox">{/if}
            {/if}
                                </td>
            {/if}
                             </tr>
                {/foreach}
                            
                            </tbody>
                        </table>  *}
                            <input type="hidden" name="nid" value="{$nId}">
                           
                     </form>
                </div>        

                  <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:200px">
                            <button type="button" id="edit"  class="gplus-blue">Edit</button>
                            <button type="button" onclick="AccessoryInsert()" class="gplus-blue">Insert</button>
                            <button type="button" id="delete" class="gplus-red">Delete</button>
                    </div>
                </div>  
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               

                    {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                    {/if} 
               
              <div style="width:100%;text-align: right">
                        <input id="inactivetick"  type="checkbox" > Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                   
                    </div>
<hr>
    <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/productSetup'">Finish</button>

    </div>
                    {if $nId!='' }
                <div>
                <hr>
                <button type="button" class="gplus-blue" onclick="saveAssigned()">Save</button>
                </div>                
                    {/if}     



                        {/block}