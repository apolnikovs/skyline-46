DROP PROCEDURE IF EXISTS trigger_insert;

DELIMITER ;;

CREATE PROCEDURE trigger_insert(IN newData TEXT, IN tableName VARCHAR(100), IN fieldName VARCHAR(100), IN primaryID INT(11), IN userID INT(11))
BEGIN

	IF userID IS NULL
	THEN SET userID = 0;
	END IF;

 	INSERT INTO audit_new 
				(
					UserID, 
					TableName,
					`Action`, 
					PrimaryID, 
					FieldName, 
					NewValue
				) 
	VALUES 		
				(
					userID,
					tableName,
					'insert',
					primaryID,
					fieldName,
					newData
				);

END
;;

DELIMITER ;