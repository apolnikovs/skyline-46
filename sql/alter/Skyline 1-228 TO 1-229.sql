# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.228');


# ---------------------------------------------------------------------- #
# Add Table service_provider_supplier                                    #
# ---------------------------------------------------------------------- #
CREATE TABLE `service_provider_supplier` (
    `ServiceProviderSupplierID` INT(10) NOT NULL AUTO_INCREMENT,
    `ServiceProviderID` INT(10) NOT NULL,
    `SupplierID` INT(10) NOT NULL,
    `CompanyName` VARCHAR(50) NOT NULL,
    `AccountNumber` VARCHAR(50) NULL DEFAULT NULL,
    `TelephoneNo` VARCHAR(20) NULL DEFAULT NULL,
    `PostCode` VARCHAR(10) NULL DEFAULT NULL,
    `FaxNo` VARCHAR(13) NULL DEFAULT NULL,
    `EmailAddress` VARCHAR(255) NULL DEFAULT NULL,
    `Website` VARCHAR(255) NULL DEFAULT NULL,
    `ContactName` VARCHAR(50) NULL DEFAULT NULL,
    `DirectOrderTemplateID` INT(11) NULL DEFAULT NULL,
    `CollectSupplierOrderNo` VARCHAR(20) NULL DEFAULT NULL,
    `PostageChargePrompt` ENUM('Yes','No') NULL DEFAULT NULL,
    `DefaultPostageCharge` DECIMAL(10,2) NULL DEFAULT NULL,
    `DefaultCurrencyID` INT(11) NULL DEFAULT NULL,
    `NormalSupplyPeriod` INT(11) NULL DEFAULT NULL,
    `MinimumOrderValue` DECIMAL(50,0) NULL DEFAULT NULL,
    `UsageHistoryPeriod` INT(50) NULL DEFAULT NULL,
    `MultiplyByFactor` DECIMAL(10,2) NULL DEFAULT NULL,
    `TaxExempt` ENUM('Yes','No') NULL DEFAULT NULL,
    `OrderPermittedVariancePercent` DECIMAL(10,2) NULL DEFAULT NULL,
    `Status` ENUM('Active','In-Active') NOT NULL DEFAULT 'Active',
    `BuildingNameNumber` VARCHAR(50) NULL DEFAULT NULL,
    `Street` VARCHAR(100) NULL DEFAULT NULL,
    `LocalArea` VARCHAR(100) NULL DEFAULT NULL,
    `TownCity` VARCHAR(100) NULL DEFAULT NULL,
    `CountryID` INT(3) NULL DEFAULT NULL,
    `ModifiedUserID` INT(11) NULL DEFAULT NULL,
    `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`ServiceProviderSupplierID`),
    INDEX `CompanyName` (`CompanyName`),
    INDEX `AccountNumber` (`AccountNumber`),
    INDEX `ServiceProviderID` (`ServiceProviderID`),
    INDEX `SupplierID` (`SupplierID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.229');