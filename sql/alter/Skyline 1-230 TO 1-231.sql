# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.230');


# ---------------------------------------------------------------------- #
# Add Table mobile_phone_network                                         #
# ---------------------------------------------------------------------- #
CREATE TABLE IF NOT EXISTS mobile_phone_network (
												 MobilePhoneNetworkID int(11) NOT NULL AUTO_INCREMENT, 
												 MobilePhoneNetworkName varchar(64) DEFAULT NULL, 
												 CountryID int(11) DEFAULT NULL, 
												 PhoneNetworkType enum('Operator','MVNO') DEFAULT NULL, 
												 CreatedDate timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
												 EndDate timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
												 Status enum('Active','In-active') NOT NULL DEFAULT 'Active', 
												 ModifiedUserID int(11) DEFAULT NULL, 
												 ModifiedDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
												 PRIMARY KEY (MobilePhoneNetworkID), 
												 KEY IDX_mobile_phone_network_ModifiedUserID_FK (ModifiedUserID)
												 ) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO mobile_phone_network (MobilePhoneNetworkID, MobilePhoneNetworkName, CountryID, PhoneNetworkType, CreatedDate, EndDate, Status, ModifiedUserID, ModifiedDate) VALUES 
(1, '3', 1, 'Operator', '2013-04-19 10:06:30', '0000-00-00 00:00:00', 'Active', 1, '2013-04-19 11:56:41'), 
(2, 'EE', 1, 'Operator', '2013-04-19 10:09:17', '0000-00-00 00:00:00', 'Active', 1, '2013-04-19 11:57:00'), 
(3, 'O2', 1, 'Operator', '2013-04-19 10:11:12', '0000-00-00 00:00:00', 'Active', 1, '2013-04-19 11:57:13'), 
(4, 'Orange', 1, 'Operator', '2013-04-19 10:41:04', '0000-00-00 00:00:00', 'Active', 1, '2013-04-19 11:57:35'), 
(5, 'Talk Mobile', 1, 'Operator', '2013-04-19 11:58:00', '0000-00-00 00:00:00', 'Active', 1, '2013-04-19 11:58:00'), 
(6, 'Tesco Mobile', 1, 'Operator', '2013-04-19 11:58:18', '0000-00-00 00:00:00', 'Active', 1, '2013-04-19 11:58:18'), 
(7, 'TMobile', 1, 'Operator', '2013-04-19 11:58:34', '0000-00-00 00:00:00', 'Active', 1, '2013-04-19 11:58:34'), 
(8, 'Virgin', 1, 'Operator', '2013-04-19 11:58:50', '0000-00-00 00:00:00', 'Active', 1, '2013-04-19 11:58:50'), 
(9, 'Vodafone', 1, 'Operator', '2013-04-19 11:59:08', '0000-00-00 00:00:00', 'Active', 1, '2013-04-19 11:59:08');


# ---------------------------------------------------------------------- #
# Modify Table job                                                       #
# ---------------------------------------------------------------------- #
ALTER TABLE job ADD MobilePhoneNetworkID INT( 11 ) NULL;
ALTER TABLE job ADD RefreshDate date NULL;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.231');