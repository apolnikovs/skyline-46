
# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- #
call UpgradeSchemaVersion('3.416');



# ---------------------------------------------------------------------- #
#  Stuart                                                   #6918  #	
# -----------------------------------------------------------------------#

UPDATE `wizard_info` SET `JSON`='{"ExportSQL":"SELECT j.JobID AS \'Job No\',IF(j.batchID IS NULL,lcd.Refference,IF(jc.BatchType=\'R\',CONCAT(\'BD\',j.batchID,\'R\'),CONCAT(\'BD\',j.batchID))) AS \'Batch No\',man.ManufacturerName AS \'Manufacturer\', ut.UnitTypeName AS \'Unit Type\',m.ModelNumber AS \'Model Number\', IF(mpn.MobilePhoneNetworkName IS NULL AND j.ManufacturerID=1,\'UNKNOWN\',mpn.MobilePhoneNetworkName) AS \'Mobile Network\', jc.UnitGrade AS \'Grade\', IF(jc.FMIPLocked=\'Yes\',\'Enabled\',\'Disabled\') AS \'FMIP Status\', j.ImeiNo AS \'IMEI\', IFNULL(ROUND((jco.TotalCost - (jco.TotalCost*n.LCDProcessingCostPerc*0.01)),2),0.00) AS \'Value\', IFNULL(rr.TargetSaleValue,0.00) AS \'Target Value\' FROM job j LEFT JOIN job_cellular jc ON jc.JobID=j.JobID LEFT JOIN manufacturer man ON man.ManufacturerID=j.ManufacturerID LEFT JOIN service_provider_model m ON m.ModelID=j.ModelID AND m.ServiceProviderID=1 AND m.Status=\'Active\' LEFT JOIN unit_type ut ON ut.UnitTypeID=m.UnitTypeID LEFT JOIN service_provider_model_recycled_value rr ON rr.ServiceProviderModelID=m.ServiceProviderModelID AND rr.Grade=jc.UnitGrade AND rr.ForMonth= MONTH(NOW()) AND rr.ForYear=YEAR(NOW()) LEFT JOIN mobile_phone_network mpn ON mpn.MobilePhoneNetworkID=j.MobilePhoneNetworkID LEFT JOIN job_cost jco ON jco.JobID=j.JobID JOIN lcd_batch lcd ON lcd.LcdBatchID=jc.BatchRefferenceID LEFT JOIN network n ON n.NetworkID=lcd.NetworkID WHERE jc.BatchRefferenceID=:LCDBatchID","Scope":true,"Params":[{"param":"LCDBatchID","condition":""}]}' WHERE  `Name`='SingleLCDBatchExport';

	
# ----------------------------------------------------------------------- #
# Update Database Schema Version No.                                      #
# ----------------------------------------------------------------------- #
INSERT into version (VersionNo) VALUES ('3.417');